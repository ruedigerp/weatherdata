from flask import Flask, jsonify, render_template, make_response, request
# , copy_current_request_context
import json
import urllib
import os
import logging
import pandas as pd
import plotly.express as px
import csv
from dotenv import load_dotenv
from flask_socketio import SocketIO
# , emit
# from time import sleep
# from random import random
from threading import Thread, Event
import shutil

load_dotenv()

app = Flask(__name__)
gunicorn_error_logger = logging.getLogger('logs/gunicorn.error')
app.logger.handlers.extend(gunicorn_error_logger.handlers)
app.logger.setLevel(logging.DEBUG)
tokenList = os.environ.get("TOKENLIST")
socketio = SocketIO(app, async_mode=None, logger=True, engineio_logger=True, cors_allowed_origins="*")

thread = Thread()
thread_stop_event = Event()


def randomNumberGenerator():
    """
    Generate a random number every 1 second and emit to a socketio instance (broadcast)
    Ideally to be run in a separate thread?
    """
    # infinite loop of magical random numbers
    print("Making random numbers")
    while not thread_stop_event.isSet():
        # number = round(random() * 10, 3)
        # print(number)
        f = open("/app/data/current_temp.dat", "r")
        number = f.read()
        f.close()
        f = open("/app/data/current_hpa.dat", "r")
        hpa = f.read()
        f.close()
        socketio.emit('newnumber', {'number': number, 'hpa': hpa}, namespace='/test')
        socketio.sleep(5)


@socketio.on('connect', namespace='/test')
def test_connect():
    # need visibility of the global thread object
    global thread
    print('Client connected')

    # Start the random number generator thread only if the thread has not been started before.
    if not thread.is_alive():
        print("Starting Thread")
        thread = socketio.start_background_task(randomNumberGenerator)


@socketio.on('disconnect', namespace='/test')
def test_disconnect():
    print('Client disconnected')


@app.route('/add/temp', methods=['PUT'])
def addTemp():
    temp = request.headers.get('X-TEMP')
    date = request.headers.get('X-DATE')
    token = request.headers.get('X-TOKEN')
    # if token == tokenList:
    if token:
        fields = [date, temp]
        t_fields = [temp]
        with open(r'/app/data/current_temp.dat', 'w') as f:
            writer = csv.writer(f)
            writer.writerow(t_fields)
        with open(r'/app/data/temp.csv', 'a') as f:
            writer = csv.writer(f)
            writer.writerow(fields)
        return jsonify({"status": "ok", "date": date, "temp": temp}), 201
    else:
        return jsonify({"status": "token not ok", "date": date, "temp": temp, "Token": token}), 403


@app.route('/add/hpa', methods=['PUT'])
def addHpa():
    hpa = request.headers.get('X-HPA')
    date = request.headers.get('X-DATE')
    token = request.headers.get('X-TOKEN')
    # if token == tokenList:
    if token:
        fields = [date, hpa]
        h_fields = [hpa]
        with open(r'/app/data/current_hpa.dat', 'w') as f:
            writer = csv.writer(f)
            writer.writerow(h_fields)
        with open(r'/app/data/hpa.csv', 'a') as f:
            writer = csv.writer(f)
            writer.writerow(fields)
        return jsonify({"status": "ok", "date": date, "hpa": hpa}), 201
    else:
        return jsonify({"status": "token not ok", "date": date, "hpa": hpa, "Token": token}), 403


@app.route('/', methods=['GET'])
def index():
    resp = make_response(render_template('index.tmpl'))
    return resp


@app.route('/get/temp', methods=['GET'])
def getTemp():
    with open(r"/app/data/temp.csv", 'r') as fp:
        for count, line in enumerate(fp):
            pass
    # print('Total Lines', count + 1)
    lastline = count - 100
    df = pd.read_csv('/app/data/temp.csv', skiprows=[i for i in range(1, lastline)])
    fig = px.line(df, x='Date', y='Temperature', title='Temperature last 100 records')
    fig.show()
    fig.write_image("/app/static/generated/temp_last3000.png")
    df = pd.read_csv('/app/data/temp.csv', skiprows=[i for i in range(1, 10)])
    fig = px.line(df, x='Date', y='Temperature', title='Temperature overview')
    fig.show()
    fig.write_image("/app/static/generated/temperature.png")
    # resp = make_response(render_template('index.tmpl'))
    # return resp
    return jsonify({"status": "ok"}), 201


@app.route('/get/hpa', methods=['GET'])
def getHpa():
    with open(r"/app/data/hpa.csv", 'r') as fp:
        for count, line in enumerate(fp):
            pass
    # print('Total Lines', count + 1)
    lastline = count - 100
    df = pd.read_csv('/app/data/hpa.csv', skiprows=[i for i in range(1, lastline)])
    fig = px.line(df, x='Date', y='HPA', title='HPA last 100 records')
    fig.show()
    fig.write_image("/app/static/generated/hpa_last3000.png")
    df = pd.read_csv('/app/data/hpa.csv', skiprows=[i for i in range(1, 10)])
    fig = px.line(df, x='Date', y='HPA', title='HPA overview')
    fig.show()
    fig.write_image("/app/static/generated/hpa.png")
    # resp = make_response(render_template('index.tmpl'))
    # return resp
    return jsonify({"status": "ok"}), 201


@app.route("/check/running")
def checkrunning():
    return jsonify({"status": "ok"}), 201


@app.route("/check/api")
def checkapi():
    req = urllib.request.Request("http://" + os.environ.get('API_HOST') + ":" + os.environ.get('API_PORT') + "/api/v1.1/database/check")
    resp = urllib.request.urlopen(req)
    data = json.loads(resp.read())
    return jsonify(data)


@app.route("/backup")
def backup():
    output_filename = "/app/static/backup"
    dir_name = "/app/data"
    shutil.make_archive(output_filename, 'zip', dir_name)
    return jsonify({"status": "ok"}), 201


if __name__ == '__main__':
    # app.run(host='127.0.0.1', port=8080, debug=False)
    app.run(port=8080, debug=False)
