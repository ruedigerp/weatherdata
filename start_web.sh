#!/bin/bash

export APP_PATH=$(pwd)
# python3 web.py


gunicorn wsgi:app \
    --workers 1 \
    --bind 0.0.0.0:8080 \
    --log-file=- \
    --log-level DEBUG \
    --reload

# --log-file gunicorn.log \
