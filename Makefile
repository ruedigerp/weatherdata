app_name = umweltdaten
imagetag = docker.tytik.cloud:5000/umweltdaten:stable
build:
	@docker build -t $(imagetag) .

build-no-cache:
	@docker build --no-cache -t $(imagetag) .

run:
	docker-compose up -d

status:
	docker-compose ps
	# @docker ps | grep $(app_name)

stop:
	docker-compose down

kill:
	@echo 'Killing Container ...'
	@docker-compose down
	# @docker ps | grep $(app_name) | awk '{print $$1}' | xargs docker stop

restart:
	echo "restart ${name}"
	docker-compose rm -sf ${name}
	docker-compose up -d --no-deps ${name}

restart-lb:
	docker-compose rm -sf cs_lb; docker-compose up -d --no-deps cs_lb

restart-web01:
	docker-compose rm -sf cs_web01; docker-compose up -d --no-deps cs_web01

restart-web02:
	docker-compose rm -sf cs_web02; docker-compose up -d --no-deps cs_web02

restart-api:
	docker-compose rm -sf cs_api_1; docker-compose up -d --no-deps cs_api

restart-api_1_1:
	docker-compose rm -sf cs_api_1_1;
	docker-compose up -d --no-deps cs_api_1_1
