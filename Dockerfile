FROM python:latest as builder
WORKDIR /app
RUN /usr/local/bin/python -m pip install --upgrade pip
COPY requirements.txt /app
RUN pip install --prefix=/install -r requirements.txt

FROM python:latest
COPY --from=builder /install /usr/local
WORKDIR /app
COPY .env /app
COPY web.py /app
COPY wsgi.py /app
COPY start_web.sh /app
COPY data /app/data
COPY static /app/static/
COPY templates /app/templates
RUN ls -la /app /app/templates
EXPOSE 8080
## RUN cd /app
## RUN gunicorn --bind 0.0.0.0:8001 wsgi:app --reload
ENTRYPOINT ["./start_web.sh"]
